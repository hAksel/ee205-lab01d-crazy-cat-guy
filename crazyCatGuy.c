///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
/// @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ summation 6
///   The sum of the digits from 1 to 6 is 21
///
/// @author  Aksel Sloan <aksel@hawaii.edu> 
/// @date    13 January 2022  
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
   int n = atoi( argv[1] );
   
   int sumStart, sum ,i;
   
   sumStart = 0;
   sum = sumStart;
   i = sumStart;

   while (i <= n){
      sum = sum + i;
      i++; //same as i = i + 1  
   }

   printf("The sum of digits from 1 to %d is %d\n", n, sum);
 
  
   
 
  
   
   return 0;
}
